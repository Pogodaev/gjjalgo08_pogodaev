package com.getjavajob.training.algo08.pogodaevp.lesson06;

import java.util.HashMap;

/**
 * Created by paul on 26.05.16.
 */
public class MatrixTask<V> implements Matrix<Integer> {
    public HashMap<KeyMatrix, Integer> matrix;

    public MatrixTask(HashMap<KeyMatrix, Integer> matrix) {
        this.matrix = matrix;
    }

    @Override
    public Integer get(int i, int j) {
        return matrix.get(new KeyMatrix(i, j));
    }

    @Override
    public void set(int i, int j, Integer value) {
        matrix.put(new KeyMatrix(i, j), value);
    }
}
