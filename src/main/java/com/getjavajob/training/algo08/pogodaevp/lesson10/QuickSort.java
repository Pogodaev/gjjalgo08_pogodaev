package com.getjavajob.training.algo08.pogodaevp.lesson10;

/**
 * Created by paul on 12.06.16.
 */
public class QuickSort {

    public static void quickSort(int[] A, int first, int second) {
        int i = first;
        int j = second;
        int x = A[first + (second - first) / 2];
        do {
            while (A[i] < x) {
                ++i;
            }
            while (A[j] > x) {
                --j;
            }
            if (i <= j) {
                int temp = A[i];
                A[i] = A[j];
                A[j] = temp;
                i++;
                j--;
            }
        } while (i <= j);

        if (first < j) {
            quickSort(A, first, j);
        }
        if (i < second) {
            quickSort(A, i, second);
        }
    }
}
