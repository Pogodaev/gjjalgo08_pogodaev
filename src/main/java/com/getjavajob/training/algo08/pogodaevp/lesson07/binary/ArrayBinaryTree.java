package com.getjavajob.training.algo08.pogodaevp.lesson07.binary;

import com.getjavajob.training.algo08.pogodaevp.lesson07.Node;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    private ArrayList<Node<E>> tree = new ArrayList<>();
    private NodeImpl<E>[] array = new NodeImpl[10000];
    private int size;

    @Override
    public Iterable<Node<E>> breadthFirst() {
        Queue<Node<E>> queue = new LinkedList<>();
        ArrayList<Node<E>> breadthList = new ArrayList<>();
        Queue<Integer> numbers = new LinkedList<>();
        if (root() == null) {
            return queue;
        }

        queue.clear();
        queue.add(array[0]);
        numbers.add(0);

        while (!queue.isEmpty()) {
            Node<E> node = queue.remove();
            int m = numbers.remove();
            breadthList.add(node);

            if (array[2 * m + 1] != null) {
                queue.add(array[2 * m + 1]);
                numbers.add(2 * m + 1);
            }
            if (array[2 * m + 2] != null) {
                queue.add(array[2 * m + 2]);
                numbers.add(2 * m + 2);
            }
        }
        return breadthList;
    }

    @Override
    public Iterable<Node<E>> inOrder() {

        int k = 0;
        ArrayList<Node<E>> inList = new ArrayList<>();

        if (root() == null) {
            return inList;
        }

        Stack<Node<E>> stack = new Stack<>();
        Stack<Integer> numbers = new Stack<>();

        Node<E> currentNode = array[0];

        while (!stack.empty() || currentNode != null) {

            if (currentNode != null) {
                stack.push(currentNode);
                numbers.push(k);
                currentNode = array[2 * k + 1];
                k = 2 * k + 1;
            } else {
                Node<E> n = stack.pop();
                k = numbers.pop();
                inList.add(n);
                currentNode = array[2 * k + 2];
                k = 2 * k + 2;
            }
        }
        return inList;
    }

    @Override
    public Iterable<Node<E>> preOrder() {
        Stack<Node<E>> stack = new Stack<>();
        Stack<Integer> numbers = new Stack<>();
        ArrayList<Node<E>> preList = new ArrayList<>();
        int k = 0;
        stack.push(array[0]);
        numbers.push(k);

        while (!stack.isEmpty()) {
            Node<E> current = stack.pop();
            k = numbers.pop();
            preList.add(current);

            if (array[2 * k + 2] != null) {
                stack.push(array[2 * k + 2]);
                numbers.push(2 * k + 2);
            }
            if (array[2 * k + 1] != null) {
                stack.push(array[2 * k + 1]);
                numbers.push(2 * k + 1);
            }
        }
        return preList;
    }

    @Override
    public Iterable<Node<E>> postOrder() {
        ArrayList<Node<E>> preList = new ArrayList<>();
        Node<E> head = root();
        int k = 0;
        LinkedList<Node<E>> stack = new LinkedList<>();
        Stack<Integer> numbers = new Stack<>();
        if (head == null) {
            return preList;
        }

        numbers.push(k);
        stack.push(root());

        while (!stack.isEmpty()) {
            Node next = stack.peek();
            int m = numbers.peek();

            boolean finishedSubtrees = (array[2 * m + 2] == head || array[2 * m + 1] == head);
            boolean isLeaf = (array[2 * m + 2] == null && array[2 * m + 1] == null);

            if (finishedSubtrees || isLeaf) {
                stack.pop();
                numbers.pop();
                preList.add(next);
                head = next;
            } else {
                if (array[2 * m + 2] != null) {
                    stack.push(array[2 * m + 2]);
                    numbers.push(2 * m + 2);
                }
                if (array[2 * m + 1] != null) {
                    stack.push(array[2 * m + 1]);
                    numbers.push(2 * m + 1);
                }
            }
        }
        return preList;
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int parentIndex = 2 * ((NodeImpl<E>) (p)).index + 1;
        return array[parentIndex];
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int parentIndex = 2 * ((NodeImpl<E>) (p)).index + 2;
        return array[parentIndex];
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        int arrayIndex = 2 * ((NodeImpl<E>) (n)).index + 1;
        if (array[arrayIndex] != null) {
            throw new IllegalArgumentException();
        }
        array[arrayIndex] = new NodeImpl<>();
        array[arrayIndex].index = arrayIndex;
        array[arrayIndex].value = e;
        size++;
        return array[arrayIndex];
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        int arrayIndex = 2 * ((NodeImpl<E>) (n)).index + 2;
        if (array[arrayIndex] != null) {
            throw new IllegalArgumentException();
        }
        array[arrayIndex] = new NodeImpl<>();
        array[arrayIndex].index = arrayIndex;
        array[arrayIndex].value = e;
        size++;
        return array[arrayIndex];
    }

    @Override
    public Node<E> root() {
        return array[0];
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int arrayIndex = ((NodeImpl<E>) n).index;
        if (arrayIndex % 2 == 0) {
            return array[(arrayIndex - 2) / 2];
        } else {
            return array[(arrayIndex - 1) / 2];
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (array[0] != null) {
            throw new IllegalStateException();
        }
        array[0] = new NodeImpl<>(0, e);

        size++;
        return array[0];
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        int arrayIndex = ((NodeImpl<E>) (n)).index;
        if (array[2 * arrayIndex + 1] == null) {
            array[2 * arrayIndex + 1] = new NodeImpl();
            array[2 * arrayIndex + 1].index = 2 * arrayIndex + 1;
            array[2 * arrayIndex + 1].value = e;
            size++;
            return array[2 * arrayIndex + 1];
        } else if (array[2 * arrayIndex + 2] == null) {
            array[2 * arrayIndex + 2] = new NodeImpl();
            array[2 * arrayIndex + 2].index = 2 * arrayIndex + 2;
            array[2 * arrayIndex + 2].value = e;
            size++;
            return array[2 * arrayIndex + 2];
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        int arrayIndex = ((NodeImpl<E>) (n)).index;
        if (array[arrayIndex] == null) {
            throw new IllegalArgumentException();
        } else {
            E val = (E) array[arrayIndex].getElement();
            array[arrayIndex].value = e;
            return val;
        }
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int arrayIndex = ((NodeImpl<E>) (n)).index;
        if (array[arrayIndex] == null) {
            throw new IllegalArgumentException();
        }
        E deleted = (E) array[arrayIndex].getElement();
        array[arrayIndex] = null;
        return deleted;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int index = 0;
            int count;

            @Override
            public boolean hasNext() {
                return count < size;
            }

            @Override
            public E next() {
                if (array[index] == null) {
                    index++;
                    return null;
                } else {
                    count++;
                }
                return array[index].getElement();
            }

            @Override
            public void remove() {
                array[index] = null;
                count--;
            }
        };
    }

    @Override
    public Iterable<Node<E>> nodes() {
        LinkedList<Node<E>> llist = new LinkedList<Node<E>>();
        LinkedList<Node<E>> extra = new LinkedList<Node<E>>();
        NodeImpl<E> node = array[0];

        llist.push(node);
        while (!llist.isEmpty()) {
            node = (NodeImpl<E>) llist.pop();
            extra.add(node);
            if (array[2 * node.index + 1] != null) {
                llist.push(array[2 * node.index + 1]);
            }
            if (array[2 * node.index + 2] != null) {
                llist.push(array[2 * node.index + 2]);
            }
        }
        return extra;
    }

    protected static class NodeImpl<E> implements Node<E> {

        public int index;
        public E value;

        public NodeImpl() {
        }

        public NodeImpl(E value) {
            this.value = value;
        }

        public NodeImpl(int index, E value) {
            this.index = index;
            this.value = value;
        }

        @Override
        public E getElement() {
            return value;
        }
    }
}
