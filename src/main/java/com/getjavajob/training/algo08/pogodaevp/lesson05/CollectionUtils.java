package com.getjavajob.training.algo08.pogodaevp.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by paul on 23.05.16.
 */

interface CollectionFilter<E> {
    public boolean check(E element);
}

interface ChangeTo<E, K> {
    public K change(E element);
}

interface TransformList<E, K> {
    public List<K> change(List<E> list);
}

interface SomeLogic<E> {
    public E forElementsDo(E listElem);
}

public class CollectionUtils<E> {

    public static <E> List<E> filter(List<E> list, CollectionFilter<E> predicate) {
        List<E> newList = new ArrayList<E>();
        for (E l : list) {
            if (!predicate.check(l)) {
                newList.add(l);
            }
        }
        return newList;
    }

    //returns new collection
    public static <E, K> List<K> transform(List<E> list, ChangeTo<E, K> rule) {
        List<K> newList = new ArrayList<K>();
        for (E l : list) {
            newList.add(rule.change(l));
        }
        return newList;
    }

    public static <E, K> List<K> transform(List<E> list, TransformList<E, K> trans) {
        return trans.change(list);
    }

    public static <E> List<E> forAllDo(List<E> list, SomeLogic<E> logic) {
        for (E item : list) {
            logic.forElementsDo(item);
        }
        return list;
    }

    public static class UnmodifableCollection<E> implements Collection<E> {
        final Collection<E> c;

        public UnmodifableCollection(Collection<E> c) {
            this.c = c;
        }

        @Override
        public int size() {
            return this.c.size();
        }

        @Override
        public boolean isEmpty() {
            return this.c.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return this.c.contains(o);
        }

        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> i;

                {
                    this.i = UnmodifableCollection.this.c.iterator();
                }

                public boolean hasNext() {
                    return this.i.hasNext();
                }

                public E next() {
                    return this.i.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        @Override
        public Object[] toArray() {
            return this.c.toArray();
        }

        @Override
        public <T> T[] toArray(T[] ts) {
            return this.c.toArray(ts);
        }

        @Override
        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
    }
}
