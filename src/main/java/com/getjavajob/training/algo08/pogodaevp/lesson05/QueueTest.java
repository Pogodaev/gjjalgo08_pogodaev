package com.getjavajob.training.algo08.pogodaevp.lesson05;

import java.util.LinkedList;
import java.util.Queue;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.fail;

/**
 * Created by paul on 23.05.16.
 */
public class QueueTest {
    public static void main(String[] args) {
        queueAddTest();
        queueOfferTest();
        queueRemoveTest();
        queuePollTest();
        queueElementTest();
        queuePeekTest();
    }

    public static void queueAddTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        assertEquals("QueueTest.queueAddTest", true, queue.add(Integer.MAX_VALUE * Integer.MAX_VALUE));
    }

    public static void queueOfferTest() {
        Queue<Integer> queue = new LinkedList<>();
        assertEquals("QueueTest.queueOfferTest", true, queue.offer(12));
    }

    public static void queueRemoveTest() {
        Queue<Integer> queue = new LinkedList<Integer>();

        String msg = "NoSuchElementException";
        try {
            queue.remove();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queueRemoveTestFail", msg, e.getClass().getSimpleName());
        }

        queue.add(12);
        try {
            queue.remove();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queueRemoveTestFail", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("QueueTest.queueRemoveTestOk", "AssertionError", e.getClass().getSimpleName());
        }
    }


    public static void queuePollTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NullPointerException";
        try {
            int k = queue.poll();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queuePollTest", msg, e.getClass().getSimpleName());
        }
    }


    public static void queueElementTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NoSuchElementException";
        try {
            int k = queue.element();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queueElementTest", msg, e.getClass().getSimpleName());
        }
    }

    public static void queuePeekTest() {
        Queue<Integer> queue = new LinkedList<Integer>();
        String msg = "NullPointerException";
        try {
            int k = queue.peek();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.queuePeekTest", msg, e.getClass().getSimpleName());
        }
    }
}
