package com.getjavajob.training.algo08.pogodaevp.lesson05;

/**
 * Created by paul on 23.05.16.
 */
public class LinkedListStack<E> implements Stack<E> {

    private int top = -1;
    private Node<E> head;

    static class Node<E> {
        public Node<E> next;
        public E val;
    }


    public void push(E val) {
        Node node = new Node();
        node.val = val;
        node.next = head;
        head = node;
    }

    @Override
    public E pop() {
        Node<E> temp = head;
        head = head.next;
        return temp.val;
    }

    @Override
    public String toString() {
        String str = "";
        Node<E> node = head;
        while (node != null) {
            str += node.val + " ";
            node = node.next;
        }
        return str;
    }
}
