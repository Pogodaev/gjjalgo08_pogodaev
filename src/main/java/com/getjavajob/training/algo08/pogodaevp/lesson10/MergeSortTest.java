package com.getjavajob.training.algo08.pogodaevp.lesson10;

import java.util.Random;

import static com.getjavajob.training.algo08.pogodaevp.lesson10.BubbleSort.sortBubble;
import static com.getjavajob.training.algo08.pogodaevp.lesson10.MergeSort.mergeSort;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 15.06.16.
 */

public class MergeSortTest {
    public static void main(String[] args) {
        testMergeSort();
    }

    private static void testMergeSort() {
        int[] array = new int[Integer.MAX_VALUE / 100];
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            array[i] = random.nextInt();
        }
        int[] cmp = sortBubble(array);
        mergeSort(array);
        assertEquals("MergeSortTest.testMergeSort", cmp, mergeSort(array));
    }
}
