package com.getjavajob.training.algo08.pogodaevp.lesson04;

import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by paul on 09.05.16.
 */
public class DoublyLinkedList<V> extends AbstractDll<V> implements List<V> {

    private int size;
    private Element<V> first;
    private Element<V> last;

    public DoublyLinkedList() {
        this.size = 0;
    }

    @Override
    public boolean add(V val) {
        if (isEmpty()) {
            Element<V> element = new Element<V>(null, null, val);
            first = element;
            last = element;
        } else {
            Element<V> element = new Element<V>(last, null, val);
            last.setNext(element);
            last = element;
        }
        size++;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void add(int i, V v) {
        if (i == size()) {
            add(v);
        } else {
            insertElement(v, iterate(i));
        }
    }

    private void insertElement(V v, Element<V> current) {
        Element<V> previous = current.getPrev();
        Element<V> inserted = new Element<V>(previous, current, v);
        current.setPrev(inserted);

        if (previous == null) {
            first = inserted;
        } else {
            previous.setNext(inserted);
        }
        size++;
    }

    private Element<V> iterate(int i) {

        if (i > (size - 1) || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + " Max: " + (size - 1) + " Min: 0");
        } else if (i < size / 2) {
            Element<V> current = first;
            for (int j = 0; j < i; j++) {
                current = current.getNext();
            }
            return current;
        } else {
            Element<V> current = last;
            for (int j = size - 1; j > i; j--) {
                current = current.getPrev();
            }
            return current;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean remove(Object o) {
        boolean bool = false;
        V v = (V) o;
        Element<V> current = first;

        for (int i = 0; i < size(); i++) {
            if (current.val.equals(v)) {
                removeElement(current);
                bool = true;
                size--;
            }
            current = current.getNext();
        }
        return bool;
    }

    @Override
    public V remove(int i) {
        Element<V> current = iterate(i);
        removeElement(current);
        size--;
        return current.getVal();
    }

    private void removeElement(Element<V> current) {
        if (current.equals(first)) {
            removeFirst();
        } else if (current.equals(last)) {
            removeLast();
        } else {
            current.getPrev().setNext(current.getNext());
            current.getNext().setPrev(current.getPrev());
        }
        removeRefs(current);
    }

    private void removeRefs(Element<V> current) {
        current.setPrev(null);
        current.setNext(null);
    }

    private Element<V> removeLast() {
        Element<V> current = last;
        last = current.getPrev();
        current.getPrev().setNext(null);
        removeRefs(current);
        return current;
    }

    public Element<V> removeFirst() {
        Element<V> current = first;
        first = current.getNext();
        return current;
    }

    @Override
    public V set(int i, V val) {
        Element<V> current = iterate(i);
        Element<V> element = new Element<V>(val);
        element.setNext(current.getNext());
        element.setPrev(current.getPrev());
        current.setNext(null);
        current.setPrev(null);
        return (V) element;
    }

    @Override
    public V get(int i) {
        if (i + 1 > size || i + 1 < 0) {
            throw new IndexOutOfBoundsException();
        }
        Element<V> current = iterate(i);
        return current.getVal();
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public boolean contains(Object e) {
        return false;
    }

    @Override
    public V[] toArray() {
        return null;
    }

    /**
     * The element class
     *
     * @param <V>
     */
    public static class Element<V> {
        Element<V> prev;
        Element<V> next;
        V val;

        public Element(V val) {
            this.val = val;
        }

        public Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }

        public Element<V> getPrev() {
            return prev;
        }

        public void setPrev(Element<V> prev) {
            this.prev = prev;
        }

        public Element<V> getNext() {
            return next;
        }

        public void setNext(Element<V> next) {
            this.next = next;
        }

        public V getVal() {
            return val;
        }

        public void setVal(V val) {
            this.val = val;
        }
    }


    public class ListIteratorImpl<V> implements ListIterator<V> {

        Element<V> lastRet = null;
        Element<V> cur = (Element<V>) first;
        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public V next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastRet = cur;
            V element = cur.val;
            cur = cur.next;
            index++;
            return element;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public V previous() {
            if (!hasPrevious()) throw new NoSuchElementException();
            cur = cur.prev;
            index--;
            lastRet = cur;
            return cur.val;
        }

        @Override
        public int nextIndex() {
            return index;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            if (lastRet == null) throw new IllegalStateException();
            Element<V> x = lastRet.prev;
            Element<V> y = lastRet.next;
            x.next = y;
            y.prev = x;
            size--;
            if (cur == lastRet) {
                cur = y;
            } else {
                index--;
            }
            lastRet = null;
        }

        @Override
        public void set(V v) {
            if (lastRet == null) throw new IllegalStateException();
            lastRet.val = v;
        }

        @Override
        public void add(V v) {
            Element<V> x = cur.prev;
            Element<V> y = new Element<V>(v);
            Element<V> z = cur;
            x.next = y;
            y.next = z;
            z.prev = y;
            y.prev = x;
            size++;
            index++;
            lastRet = null;
        }
    }
}
