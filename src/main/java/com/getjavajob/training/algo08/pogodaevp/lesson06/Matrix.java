package com.getjavajob.training.algo08.pogodaevp.lesson06;

/**
 * Created by paul on 26.05.16.
 */
public interface Matrix<V> {
    V get(int i, int j);

    void set(int i, int j, V value);
}
