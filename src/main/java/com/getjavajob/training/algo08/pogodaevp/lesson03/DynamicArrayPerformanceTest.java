package com.getjavajob.training.algo08.pogodaevp.lesson03;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.pogodaevp.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.pogodaevp.util.StopWatch.start;

/**
 * Created by paul on 09.05.16.
 */
public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {

        DynamicArray<Number> numberDynamicArray = new DynamicArray<>(10000000);
        for (int i = 0; i < 10000000; i++) {
            numberDynamicArray.add(i);
        }
        ArrayList<Number> numbers = new ArrayList<>(10000000);
        for (int i = 0; i < 10000000; i++) {
            numbers.add(i);
        }

        System.out.println("Addition/remove to/from the beginning test");
        System.out.println("==============================");
        start();
        numberDynamicArray.add(0, 147);
        System.out.println("DynamicArray.add(e): " + getElapsedTime());
        start();
        numbers.add(0, 147);
        System.out.println("ArrayList.add(e): " + getElapsedTime());
        System.out.println("------------");
        start();
        numberDynamicArray.remove(0);
        System.out.println("DynamicArray.remove(e): " + getElapsedTime());
        start();
        numbers.remove(0);
        System.out.println("ArrayList.remove(e): " + getElapsedTime());
        System.out.println("------------\n");

        System.out.println("Addition/remove to/from the middle test");
        System.out.println("==============================");
        start();
        numberDynamicArray.add(5000000, 147);
        System.out.println("DynamicArray.add(e): " + getElapsedTime());
        start();
        numbers.add(5000000, 147);
        System.out.println("ArrayList.add(e): " + getElapsedTime());
        System.out.println("------------");
        start();
        numberDynamicArray.remove(5000000);
        System.out.println("DynamicArray.remove(e): " + getElapsedTime());
        start();
        numbers.remove(5000000);
        System.out.println("ArrayList.remove(e): " + getElapsedTime());
        System.out.println("------------");

        System.out.println("Addition/remove to/from the end test");
        System.out.println("==============================");
        start();
        numberDynamicArray.add(numberDynamicArray.size() - 1, 147);
        System.out.println("DynamicArray.add(e): " + getElapsedTime());
        start();
        numbers.add(numbers.size() - 1, 147);
        System.out.println("ArrayList.add(e): " + getElapsedTime());
        System.out.println("------------");
        start();
        numberDynamicArray.remove(numberDynamicArray.size() - 1);
        System.out.println("DynamicArray.remove(e): " + getElapsedTime());
        start();
        numbers.remove(numbers.size() - 1);
        System.out.println("ArrayList.remove(e): " + getElapsedTime());
        System.out.println("------------");
    }
}
