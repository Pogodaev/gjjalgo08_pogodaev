package com.getjavajob.training.algo08.pogodaevp.lesson06;

import com.getjavajob.training.algo08.pogodaevp.util.Assert;

import java.util.HashMap;
import java.util.Set;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class MapTest {
    public static void main(String[] args) {
        HashMap<Integer, Integer> map = new HashMap<>();

        assertEquals("MapTest.SizeTest", 0, map.size());
        assertEquals("MapTest.IsEmptyTest", true, map.isEmpty());
        map.put(1, 4);
        assertEquals("MapTest.ContainsKey", true, map.containsKey(1));
        assertEquals("MapTest.ContainsValue", true, map.containsValue(4));
        assertEquals("MapTest.getValue", 4, map.get(1));
        assertEquals("MapTest.putValue", 4, map.put(1, 7));
        map.put(0, 27);
        assertEquals("MapTest.Remove", 27, map.remove(0));
        HashMap<Integer, Integer> newMap = new HashMap<>();
        newMap.put(7, 34);
        newMap.put(2, 56);
        map.putAll(newMap);
        assertEquals("MapTest.putAll", 3, map.size());
        newMap.clear();
        assertEquals("MapTest.clear", 0, newMap.size());
        Set<Integer> set = map.keySet();
        Integer[] test2 = {7, 56, 34};
        Integer[] my = new Integer[3];
        map.values().toArray(my);
        Assert.assertEquals("Maptest.Values", my, test2);
    }
}
