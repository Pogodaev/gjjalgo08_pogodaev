package com.getjavajob.training.algo08.pogodaevp.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.fail;

/**
 * Created by paul on 23.05.16.
 */
public class DequeTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testGetFirst();
        testGetLast();
        testElement();
        testPeekFirst();
        testPeekLast();
        testPollFirst();
        testPollLast();
        testPopPush();
        testRemove();
        testRemoveLast();
        testRemoveLastOccurrence();
    }

    private static void testRemoveLastOccurrence() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(12);
        arrayDeque.add(13);
        assertEquals("DequeTest.testRemoveLastOccurrence", true, arrayDeque.removeLastOccurrence(12));
    }

    private static void testRemoveLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        assertEquals("DequeTest.testRemoveLast", 13, arrayDeque.removeLast());
        String msg = "NoSuchElementException";
        try {
            arrayDeque.removeLast();
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testRemoveLastException", msg, e.getClass().getSimpleName());
        }
    }

    private static void testRemove() {
        Deque<Number> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        assertEquals("DequeTest.testRemove", true, arrayDeque.remove(13));

        String msg = "NoSuchElementException";
        try {
            arrayDeque.remove();
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testRemoveException", msg, e.getClass().getSimpleName());
        }
    }

    //the same as addFirstMethod
    private static void testPopPush() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.push(12);
        assertEquals("DequeTest.testPopPush", 12, arrayDeque.pop());
    }

    private static void testPollLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("DequeTest.testPollLast", 12, arrayDeque.pollLast());
        assertEquals("DequeTest.testPollLastSizeAfterPoll", 1, arrayDeque.size());
    }

    private static void testPollFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("DequeTest.testPollFirst", 13, arrayDeque.pollFirst());
        assertEquals("DequeTest.testPollFirstSizeAfterPoll", 1, arrayDeque.size());
    }

    private static void testPeekLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("DequeTest.testPeekLast", 12, arrayDeque.peekLast());
    }

    private static void testPeekFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("DequeTest.testPeekFirst", 13, arrayDeque.peekFirst());
    }

    //the same as addFirstMethod
    private static void testElement() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        assertEquals("DequeTest.testElement", 13, arrayDeque.element());
    }

    private static void testGetLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(12);
        assertEquals("DequeTest.testGetLast", 12, arrayDeque.getLast());
        arrayDeque.remove();
        String msg = "NoSuchElementException";
        try {
            arrayDeque.getLast();
            fail(msg);
        } catch (NoSuchElementException e) {
            assertEquals("DequeTest.testGetLastException", msg, e.getClass().getSimpleName());
        }
    }

    private static void testGetFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        assertEquals("DequeTest.testGetFirst", 13, arrayDeque.getFirst());
        arrayDeque.remove();
        String msg = "NoSuchElementException";
        try {
            arrayDeque.getFirst();
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testGetFirstException", msg, e.getClass().getSimpleName());
        }
    }

    public static void testAddFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.addFirst(12);
        assertEquals("DequeTest.testAddFirst", 12, arrayDeque.getFirst());

        String msg = "NullPointerException";
        try {
            arrayDeque.addFirst(null);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testAddFirstException", msg, e.getClass().getSimpleName());
        }
    }

    public static void testAddLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.addLast(12);
        assertEquals("DequeTest.testAddLast", 12, arrayDeque.getLast());

        String msg = "NullPointerException";
        try {
            arrayDeque.addLast(null);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testAddLastException", msg, e.getClass().getSimpleName());
        }
    }

    //the same as addFirst
    public static void testOfferFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.offer(15);
        arrayDeque.offerFirst(14);
        assertEquals("DequeTest.testOfferFirst", 14, arrayDeque.getFirst());
        assertEquals("DequeTest.testOfferFirstBool", true, arrayDeque.offerFirst(1));
    }

    public static void testOfferLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.offer(15);
        arrayDeque.offerLast(14);
        assertEquals("DequeTest.testOfferLast", 14, arrayDeque.getLast());
        assertEquals("DequeTest.testOfferLastBool", true, arrayDeque.offerLast(1));
    }
}

