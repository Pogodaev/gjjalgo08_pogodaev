package com.getjavajob.training.algo08.pogodaevp.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by paul on 09.05.16.
 */
public class DynamicArray<E> extends AbstractList<E> {
    private int modCount;
    private E[] elements;
    private int quantityOfElements;

    public DynamicArray(int n) {
        elements = (E[]) new Object[n];
        quantityOfElements = 0;
    }

    public DynamicArray() {
        this(10);
    }

    @Override
    public boolean add(E e) {
        if (elements.length > quantityOfElements && elements[quantityOfElements] == null) {
            elements[quantityOfElements] = e;
            quantityOfElements++;
            modCount++;
            return true;
        } else {
            maxArray();
            return false;
        }
    }

    private void maxArray() {
        int newLength = (int) (elements.length * 1.5);
        elements = Arrays.copyOf(elements, newLength);
    }

    @Override
    public void add(int i, E e) {
        if (quantityOfElements >= elements.length) {
            maxArray();
        }
        System.arraycopy(elements, i, elements, i + 1, quantityOfElements - i);
        elements[i] = e;
        quantityOfElements++;
        modCount++;
    }

    @Override
    public E set(int i, E e) {
        E prev = elements[i];
        elements[i] = e;
        return prev;
    }

    @Override
    public E get(int i) {
        return elements[i];
    }

    @Override
    public E remove(int i) {
        E removed = elements[i];
        if (i < quantityOfElements - 1) {
            System.arraycopy(elements, i + 1, elements, i, quantityOfElements - i - 1);
        } else if (i == quantityOfElements) {
            elements[quantityOfElements] = null;
        }
        quantityOfElements--;
        modCount++;
        return removed;
    }

    @Override
    public boolean remove(Object e) {
        boolean b = false;
        for (int i = 0; i < quantityOfElements; i++) {
            if (elements[i].equals(e)) {
                remove(i);
                b = true;
            }
        }
        modCount++;
        return b;
    }

    @Override
    public int size() {
        return quantityOfElements;
    }

    @Override
    public int indexOf(Object e) {
        int k = -1;
        for (int i = 0; i < quantityOfElements; i++) {
            if (elements[i].equals(e)) {
                k = i;
                break;
            }
        }
        return k;
    }

    @Override
    public boolean contains(Object e) {
        for (int i = 0; i < quantityOfElements; i++) {
            if (elements[i].equals(e)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public E[] toArray() {
        return elements;
    }

    public DynamicArray.ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    public class ListIteratorImpl implements ListIterator {
        int cursor;
        int lastRet;
        int expectedModifications;

        public ListIteratorImpl(int index) {
            this.cursor = index;
            this.lastRet = -1;
            this.expectedModifications = DynamicArray.this.modCount;
        }

        public boolean hasNext() {
            return cursor != DynamicArray.this.size();
        }

        public E next() {
            this.checkForComodification();
            int i = cursor;
            E[] elementData = DynamicArray.this.elements;
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            } else {
                this.cursor = i + 1;
                return elementData[this.lastRet = i];
            }
        }

        public boolean hasPrevious() {
            return this.cursor != 0;
        }

        public E previous() {
            this.checkForComodification();
            int i = this.cursor - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            } else {
                E[] elementData = DynamicArray.this.elements;
                if (i >= elementData.length) {
                    throw new ConcurrentModificationException();
                } else {
                    this.cursor = i;
                    return elementData[this.lastRet = i];
                }
            }
        }

        public int nextIndex() {
            return this.cursor;
        }

        public int previousIndex() {
            return this.cursor - 1;
        }

        public void remove() {
            this.checkForComodification();
            try {
                DynamicArray.this.remove(this.lastRet);
                this.cursor = this.lastRet;
                this.lastRet = -1;
                this.expectedModifications = DynamicArray.this.modCount;
            } catch (IndexOutOfBoundsException var2) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void set(Object e) {
            this.checkForComodification();
            try {
                DynamicArray.this.set(this.lastRet, (E) e);
            } catch (IndexOutOfBoundsException var3) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(Object e) {
            this.checkForComodification();

            try {
                int ex = this.cursor;
                DynamicArray.this.add(ex, (E) e);
                this.cursor = ex + 1;
                this.lastRet = -1;
            } catch (IndexOutOfBoundsException var3) {
                throw new ConcurrentModificationException();
            }
        }

        public void checkForComodification() {
            if (DynamicArray.this.modCount != this.expectedModifications) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
