package com.getjavajob.training.algo08.pogodaevp.lesson06;

/**
 * Created by paul on 27.05.16.
 */
public class KeyMatrix<V> {
    private int i;
    private int j;
    private int hashVal;

    public KeyMatrix(int i, int j) {
        this.i = i;
        this.j = j;
        hashVal = hashCode();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyMatrix<?> keyMatrix = (KeyMatrix<?>) o;

        if (i != keyMatrix.i) return false;
        return j == keyMatrix.j;

    }

    @Override
    public int hashCode() {
        int result = i;
        result = 31 * result + j;
        return result;
    }
}
