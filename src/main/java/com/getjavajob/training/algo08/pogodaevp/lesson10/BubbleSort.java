package com.getjavajob.training.algo08.pogodaevp.lesson10;

/**
 * Created by paul on 12.06.16.
 */
public class BubbleSort {

    public static int[] sortBubble(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 1; j < arr.length - i; j++) {
                if (arr[j - 1] > arr[j]) {
                    int temp;
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }
}
