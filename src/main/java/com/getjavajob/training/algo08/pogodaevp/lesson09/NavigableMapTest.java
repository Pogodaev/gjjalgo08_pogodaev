package com.getjavajob.training.algo08.pogodaevp.lesson09;

/**
 * Created by paul on 10.08.16.
 */

import java.util.*;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

public class NavigableMapTest {

    public static void main(String[] args) {
        testCeilingEntry();
        testCeilingKey();
        testDescendingKeySet();
        testDescendingMap();
        testFirstEntry();
        testFloorEntry();
        testFloorKey();
        testHeadMap();
        testHigherEntry();
        testHigherKey();
        testLastEntry();
        testLowerEntry();
        testNavigableKeySet();
        testPollFirstEntry();
    }

    private static void testPollFirstEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        NavigableMap<Integer, String> nap = new TreeMap<>();
        nap.put(1, "one");
        Map.Entry<Integer, String> entry = nap.firstEntry();
        assertEquals("NavigableMapTest.testPollFirstEntry", entry, map.pollFirstEntry());
    }

    private static void testNavigableKeySet() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        NavigableSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        assertEquals("NavigableMapTest.testNavigableKeySet", set, map.navigableKeySet());
    }

    private static void testLowerEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testLowerEntry", "one", map.firstEntry().getValue());
    }

    private static void testLastEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testLastEntry", "three", map.lastEntry().getValue());
    }

    private static void testHigherKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testHigherKey", 3, map.higherKey(2));
    }

    private static void testHigherEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testHigherEntry", "three", map.higherEntry(2).getValue());
    }

    private static void testHeadMap() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        NavigableMap<Integer, String> nap = new TreeMap<>(Collections.reverseOrder());
        nap.put(1, "one");
        nap.put(2, "two");
        nap.put(3, "three");
        assertEquals("NavigableMapTest.testHeadMap", nap, map.headMap(3, true));
    }

    private static void testFloorKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testFloorKey", 3, map.floorKey(3));
    }

    private static void testFloorEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testFloorEntry", "one", map.firstEntry().getValue());
    }

    private static void testFirstEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testFirstEntry", "one", map.firstEntry().getValue());
    }

    private static void testDescendingMap() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        NavigableMap<Integer, String> nap = new TreeMap<>(Collections.reverseOrder());
        nap.put(1, "one");
        nap.put(2, "two");
        nap.put(3, "three");
        assertEquals("NavigableMapTest.testDescendingMap", nap, map.descendingMap());
    }

    private static void testDescendingKeySet() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");

        NavigableSet<Integer> set = new TreeSet<>(Collections.reverseOrder());
        set.add(1);
        set.add(2);
        set.add(3);

        assertEquals("NavigableMapTest.testDescendingKeySet", set, map.descendingKeySet());
    }

    private static void testCeilingKey() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testCeilingKey", 3, map.ceilingKey(3));
    }

    private static void testCeilingEntry() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("NavigableMapTest.testCeilingEntry", "two", map.ceilingEntry(2).getValue());
    }
}
