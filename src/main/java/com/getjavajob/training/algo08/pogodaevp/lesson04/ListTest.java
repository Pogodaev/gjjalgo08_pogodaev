package com.getjavajob.training.algo08.pogodaevp.lesson04;

import java.util.ArrayList;
import java.util.Iterator;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 23.05.16.
 */
public class ListTest {
    public static void main(String[] args) {
        testGet();
        testSet();
        testAdd();
        testRemove();
        testIndexOf();
        testLastIndexOf();
        testIterator();
    }

    private static void testIterator() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(13);
        list.add(14);
        Iterator<Integer> it = list.iterator();
        Iterator<Integer> it1 = list.listIterator(1);
        assertEquals("ListTest.testIterator", 12, it.next());
        assertEquals("ListTest.testIteratorWithIndex", 13, it1.next());
    }

    private static void testLastIndexOf() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(12);
        assertEquals("ListTest.testLastIndexOf", 1, list.lastIndexOf(12));
    }

    private static void testIndexOf() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        assertEquals("ListTest.testIndexOf", 0, list.indexOf(12));
    }

    private static void testRemove() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        assertEquals("ListTest.testRemove", 12, list.remove(0));
    }

    private static void testAdd() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(0, 12);
        assertEquals("ListTest.testAdd", 12, list.get(0));
    }

    private static void testSet() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        assertEquals("ListTest.testSet", 12, list.set(0, 14));
    }

    private static void testGet() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(12);
        assertEquals("ListTest.testGet", 12, list.get(0));
    }
}
