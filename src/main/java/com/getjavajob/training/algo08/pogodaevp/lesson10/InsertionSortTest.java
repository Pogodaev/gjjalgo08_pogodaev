package com.getjavajob.training.algo08.pogodaevp.lesson10;

import static com.getjavajob.training.algo08.pogodaevp.lesson10.InsertionSort.doInsertionSort;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 15.06.16.
 */
public class InsertionSortTest {
    public static void main(String[] args) {
        testInsertionSort();
    }

    private static void testInsertionSort() {
        int[] sorted = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] unSort = new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] sort = doInsertionSort(unSort);
        assertEquals("InsertionSortTest.testInsertionSort", sorted, sort);
    }
}
