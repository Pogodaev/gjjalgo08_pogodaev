package com.getjavajob.training.algo08.pogodaevp.lesson04;

import java.util.LinkedList;

import static com.getjavajob.training.algo08.pogodaevp.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.pogodaevp.util.StopWatch.start;

/**
 * Created by paul on 19.05.16.
 */
public class DoublyLinkedListPerfomanceTest {
    public static void main(String[] args) {
        DoublyLinkedList<Number> doublyLinkedList = new DoublyLinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            doublyLinkedList.add(i);
        }
        LinkedList<Number> numbers = new LinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            numbers.add(i);
        }

        System.out.println("Addition/remove to/from the beginning test");
        System.out.println("==============================");
        start();
        doublyLinkedList.add(0, 147);
        System.out.println("DLL.add(e): " + getElapsedTime());
        start();
        numbers.add(0, 147);
        System.out.println("LinkedList.add(e): " + getElapsedTime());
        System.out.println("------------");
        start();
        doublyLinkedList.remove(0);
        System.out.println("DLL.remove(e): " + getElapsedTime());
        start();
        numbers.remove(0);
        System.out.println("LinkedList.remove(e): " + getElapsedTime());
        System.out.println("------------\n");

        System.out.println("Addition/remove to/from the middle test");
        System.out.println("==============================");
        start();
        doublyLinkedList.add(5000000, 147);
        System.out.println("DLL.add(e): " + getElapsedTime());
        start();
        numbers.add(5000000, 147);
        System.out.println("LinkedList.add(e): " + getElapsedTime());
        System.out.println("------------");
        start();
        doublyLinkedList.remove(500000);
        System.out.println("DLL.remove(e): " + getElapsedTime());
        start();
        numbers.remove(500000);
        System.out.println("LinkedList.remove(e): " + getElapsedTime());
        System.out.println("------------");

        System.out.println("Addition/remove to/from the end test");
        System.out.println("==============================");
        start();
        doublyLinkedList.add(doublyLinkedList.size() - 1, 147);
        System.out.println("DLL.add(e): " + getElapsedTime());
        start();
        numbers.add(numbers.size() - 1, 147);
        System.out.println("LinkedList.add(e): " + getElapsedTime());
        System.out.println("------------");
        start();
        doublyLinkedList.remove(doublyLinkedList.size() - 1);
        System.out.println("DLL.remove(e): " + getElapsedTime());
        start();
        numbers.remove(numbers.size() - 1);
        System.out.println("LinkedList.remove(e): " + getElapsedTime());
        System.out.println("------------");
    }
}
