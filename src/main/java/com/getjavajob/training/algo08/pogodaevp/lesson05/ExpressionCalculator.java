package com.getjavajob.training.algo08.pogodaevp.lesson05;

import java.util.Stack;

/**
 * Created by paul on 23.05.16.
 */
public class ExpressionCalculator {

    public String fromInfixtoPostfix(String infix) {
        final String operations = "-+/*^|";
        StringBuilder sb = new StringBuilder();
        LinkedListQueue<String> q = new LinkedListQueue<String>();
        Stack<Integer> s = new Stack<>();

        for (String token : infix.split(" ")) {
            if (token.isEmpty()) {
                continue;
            }
            char c = token.charAt(0);
            int idx = operations.indexOf(c);

            // check for operator
            if (idx != -1) {
                if (s.isEmpty()) {
                    s.push(idx);
                } else {
                    while (!s.isEmpty()) {
                        int prec2 = s.peek() / 2;
                        int prec1 = idx / 2;
                        if (prec2 > prec1 || (prec2 == prec1 && c != '^')) {
                            sb.append(operations.charAt(s.pop())).append(' ');
                        } else {
                            break;
                        }
                    }
                    s.push(idx);
                }
            } else if (c == '(') {
                s.push(-2); // -2 stands for '('
            } else if (c == ')') {
                // until '(' on stack, pop operators.
                while (s.peek() != -2) {
                    sb.append(operations.charAt(s.pop())).append(' ');
                }
                s.pop();
            } else {
                sb.append(token).append(' ');
            }
        }
        while (!s.isEmpty()) {
            sb.append(operations.charAt(s.pop())).append(' ');
        }
        return sb.toString();
    }

    public int calculate(String q) {
        LinkedListStack<Integer> stack = new LinkedListStack<Integer>();
        for (String item : q.split(" ")) {
            if (isNumeric(item)) {
                stack.push(Integer.parseInt(item));
            } else {
                int right = stack.pop();
                int left = stack.pop();

                if (item.equals("+")) stack.push(left + right);
                else if (item.equals("-")) stack.push(left - right);
                else if (item.equals("^")) stack.push((int) (Math.pow(left, right)));
                else if (item.equals("*")) stack.push(left * right);
                else if (item.equals("/")) stack.push(left / right);
                else if (item.equals("|")) stack.push(left | right);
            }
        }
        return stack.pop();
    }

    private static boolean isNumeric(String s) {
        try {
            Integer d = Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
