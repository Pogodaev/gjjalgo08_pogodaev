package com.getjavajob.training.algo08.pogodaevp.lesson08.balanced;

import com.getjavajob.training.algo08.pogodaevp.lesson07.Node;
import com.getjavajob.training.algo08.pogodaevp.lesson08.search.BinarySearchTree;

import java.util.ArrayList;
import java.util.List;

public class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Relinks a parent with child node
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (!makeLeftChild) { //left rotation
            if (parent == root) {
                child.parent = null;
                root = child;
            } else {
                if (parent.parent.left == parent) {
                    parent.parent.left = child;
                    child.parent = parent.parent;
                } else {
                    parent.parent.right = child;
                    child.parent = parent.parent;
                }
            }
            child.left.parent = parent;
            parent.parent = child;
            parent.right = child.left;
            child.left = parent;
        } else {
            if (parent == root) {
                child.parent = null;
                root = child;
            } else {
                if (parent.parent.left == parent) {
                    parent.parent.left = child;
                    child.parent = parent.parent;
                } else {
                    parent.parent.right = child;
                    child.parent = parent.parent;
                }
            }
            parent.parent = child;
            child.right.parent = parent;
            parent.left = child.right;
            child.right = parent;
        }
    }

    /**
     * Rotates n with parent.
     */
    public void rotate(Node<E> n) {
        NodeImpl<E> node = (NodeImpl<E>) n;
        boolean left = compare(n.getElement(), node.parent.getElement()) < 0;
        relink(node.parent, node, left);
    }

    /**
     * Performs a left-right/right-left rotations.
     */
    public Node<E> rotateTwice(Node<E> n) {
        NodeImpl<E> node = (NodeImpl<E>) n;
        if (node.parent != null && node.parent.parent != null) {
            boolean left = compare(n.getElement(), node.parent.getElement()) < 0;
            boolean leftRight = compare(node.parent.getElement(), node.parent.parent.getElement()) < 0;
            relink(node.parent, node, left);
            relink(node.parent, node, leftRight);
        }
        return null;
    }


    public void printPlease(NodeImpl<E> head) {
        List<NodeImpl<E>> list = new ArrayList<NodeImpl<E>>();
        list.add(head);
        printTree(list, getHeight(head));
    }

    public int getHeight(NodeImpl<E> head) {
        if (head == null) {
            return 0;
        } else {
            return 1 + Math.max(getHeight(head.left), getHeight(head.right));
        }
    }

    public String toString(NodeImpl<E> node) {
        String str = "";
        if (node == null) {
            return str;
        }
        str += node.getElement();
        str += " (" + toString(node.left) + ") (" + toString(node.right) + ")";
        return str;
    }

    private void printTree(List<NodeImpl<E>> levelNodes, int level) {

        List<NodeImpl<E>> nodes = new ArrayList<NodeImpl<E>>();

        //indentation for first node in given level
        printIndentForLevel(level);

        for (NodeImpl<E> treeNode : levelNodes) {

            //print node data
            System.out.print(treeNode == null ? " " : treeNode.getElement());

            //spacing between nodes
            printSpacingBetweenNodes(level);

            //if its not a leaf node
            if (level > 1) {
                nodes.add(treeNode == null ? null : treeNode.left);
                nodes.add(treeNode == null ? null : treeNode.right);
            }
        }
        System.out.println();

        if (level > 1) {
            printTree(nodes, level - 1);
        }
    }

    private void printIndentForLevel(int level) {
        for (int i = (int) (Math.pow(2, level - 1)); i > 0; i--) {
            System.out.print(" ");
        }
    }

    private void printSpacingBetweenNodes(int level) {
        //spacing between nodes
        for (int i = (int) ((Math.pow(2, level - 1)) * 2) - 1; i > 0; i--) {
            System.out.print(" ");
        }
    }

}
