package com.getjavajob.training.algo08.pogodaevp.lesson05;

/**
 * Created by paul on 23.05.16.
 */
public interface Stack<E> {
    void push(E e); // add element to the top

    E pop(); // removes element from the top
}
