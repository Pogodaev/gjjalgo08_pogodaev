package com.getjavajob.training.algo08.pogodaevp.lesson05;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        testExpressionCalculator();
    }

    private static void testExpressionCalculator() {
        ExpressionCalculator calc = new ExpressionCalculator();
        String infix = "( 2 ^ 3  + ( ( 3 + 4 ) * ( 5 * 6 ) ) ) ";
        assertEquals("ExpressionCalculatorTest.testExpressionCalculator", 218, calc.calculate(calc.fromInfixtoPostfix(infix)));
    }
}
