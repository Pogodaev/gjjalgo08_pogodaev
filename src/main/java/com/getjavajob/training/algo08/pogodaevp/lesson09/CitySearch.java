package com.getjavajob.training.algo08.pogodaevp.lesson09;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by paul on 08.06.16.
 */
public class CitySearch {
    public static void main(String[] args) {
        TreeSet<String> treeSet = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o, String t1) {
                return o.compareToIgnoreCase(t1);
            }
        });

        treeSet.add("Москва");
        treeSet.add("Могилев");
        treeSet.add("Бугульма");
        System.out.println("Hello, now we will test this TreeSet");
        SortedSet<String> result = treeSet.subSet("мо", true, "мо" + (Character.MAX_VALUE), true);
        for (String r : result) {
            System.out.println(r);
        }
    }
}
