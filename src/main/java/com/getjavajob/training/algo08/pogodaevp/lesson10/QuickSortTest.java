package com.getjavajob.training.algo08.pogodaevp.lesson10;

import static com.getjavajob.training.algo08.pogodaevp.lesson10.QuickSort.quickSort;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 11.08.16.
 */
public class QuickSortTest {
    public static void main(String[] args) {
        testQuickSort();
    }

    private static void testQuickSort() {
        int[] sorted = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] unSort = new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1};
        quickSort(unSort, 0, 8);
        assertEquals("QuickSortTest.testQuickSort", sorted, unSort);
    }
}
