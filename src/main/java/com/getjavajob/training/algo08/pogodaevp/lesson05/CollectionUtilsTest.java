package com.getjavajob.training.algo08.pogodaevp.lesson05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.algo08.pogodaevp.lesson05.CollectionUtils.*;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.fail;

/**
 * Created by paul on 21.07.16.
 */
public class CollectionUtilsTest {
    public static void main(String[] args) {
        testUnmodifiableCollection();
        testFilterMethod();
        testTransformFirst();
        testTransformSecond();
        testForAllDo();
    }

    private static void testUnmodifiableCollection() {
        String msg = "UnsupportedOperationException";
        Employee em1 = new Employee("Pasha", 20000);
        Employee em2 = new Employee("Masha", 30000);
        Employee em3 = new Employee("Ivan", 40000);

        List<Employee> list = new ArrayList<>(Arrays.asList(em1, em2, em3));
        CollectionUtils.UnmodifableCollection col = new CollectionUtils.UnmodifableCollection(list);
        try {
            col.add(new Employee("Me", 5000));
            fail(msg);
        } catch (Exception e) {
            assertEquals("CollectionUtilsTest.testUnmodifiableCollection", msg, e.getClass().getSimpleName());
        }
    }

    private static void testForAllDo() {
        Employee em1 = new Employee("Pasha", 20000);
        Employee em2 = new Employee("Masha", 30000);
        Employee em3 = new Employee("Ivan", 40000);
        List<Employee> list = new ArrayList<>(Arrays.asList(em1, em2, em3));

        SomeLogic<Employee> logic = new SomeLogic<Employee>() {
            @Override
            public Employee forElementsDo(Employee listElem) {
                int sallary = listElem.getSalary();
                listElem.setSalary(sallary * 10);
                return listElem;
            }
        };
        List<String> cmp = new ArrayList<>(Arrays.asList("Pasha 200000", "Masha 300000", "Ivan 400000"));
        assertEquals("CollectionUtilsTest.testForAllDo", cmp.toString(), forAllDo(list, logic).toString());
    }

    private static void testTransformSecond() {
        Employee em1 = new Employee("Pasha", 20000);
        Employee em2 = new Employee("Masha", 30000);
        Employee em3 = new Employee("Ivan", 40000);
        List<Employee> list = new ArrayList<>(Arrays.asList(em1, em2, em3));

        TransformList<Employee, String> trans = new TransformList<Employee, String>() {
            @Override
            public List<String> change(List<Employee> list) {
                List<String> b = (List<String>) (List<?>) list;
                return b;
            }
        };
        List<String> names = new ArrayList<>();
        for (Employee e : list) {
            names.add(e.getName() + " " + e.getName());
        }
        assertEquals("CollectionUtilsTest.testTransformSecond", names, transform(list, trans));
    }

    private static void testTransformFirst() {
        Employee em1 = new Employee("Pasha", 20000);
        Employee em2 = new Employee("Masha", 30000);
        Employee em3 = new Employee("Ivan", 40000);
        List<Employee> list = new ArrayList<>(Arrays.asList(em1, em2, em3));

        ChangeTo<Employee, String> change = new ChangeTo<Employee, String>() {
            @Override
            public String change(Employee element) {
                return element.getName();
            }
        };
        List<String> names = new ArrayList<>(Arrays.asList(em1.getName(), em2.getName(), em3.getName()));
        assertEquals("CollectionUtilsTest.testTransformFirst", names, transform(list, change));
    }


    /**
     * filters collection by applying filter conditions for each element and
     * returns the result of whether the collection was modified.
     * e.g. filter only Ivanovs from Employees list.
     */
    public static void testFilterMethod() {
        Employee em1 = new Employee("Pasha", 20000);
        Employee em2 = new Employee("Masha", 30000);
        Employee em3 = new Employee("Ivan", 40000);

        List<Employee> list = new ArrayList<>(Arrays.asList(em1, em2, em3));

        CollectionFilter fil = new CollectionFilter() {
            @Override
            public boolean check(Object element) {
                return ((Employee) element).getName().toLowerCase().contains("ivan");
            }
        };

        List<Employee> res = filter(list, fil);
        String names = "";
        for (Employee e : res) {
            names += e.getName() + " ";
        }
        assertEquals("CollectionUtilsTest.testFilter", names, "Pasha Masha ");
    }

    public static class Employee {
        private String name;
        private int salary;

        public Employee(String name, int sallary) {
            this.name = name;
            this.salary = sallary;
        }

        public String toString() {
            return name + " " + salary;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

    }

}
