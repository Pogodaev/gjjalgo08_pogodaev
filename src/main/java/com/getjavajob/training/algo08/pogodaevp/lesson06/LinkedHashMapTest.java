package com.getjavajob.training.algo08.pogodaevp.lesson06;

import java.util.LinkedHashMap;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class LinkedHashMapTest {
    public static void main(String[] args) {
        testAdd();
    }

    private static void testAdd() {
        LinkedHashMap<Integer, String> hashMap = new LinkedHashMap();
        hashMap.put(0, "Zero");
        hashMap.put(1, "One");
        hashMap.put(2, "Two");

        String[] cmp = new String[]{"Zero", "One", "Two"};
        assertEquals("LinkedHashMapTest.testAdd", cmp, hashMap.keySet().toArray(new String[0]));
    }
}
