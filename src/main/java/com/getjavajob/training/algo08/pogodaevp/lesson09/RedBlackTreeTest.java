package com.getjavajob.training.algo08.pogodaevp.lesson09;

import com.getjavajob.training.algo08.pogodaevp.lesson07.binary.LinkedBinaryTree.NodeImpl;
import com.getjavajob.training.algo08.pogodaevp.lesson09.balanced.RedBlackTree;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class RedBlackTreeTest {
    public static void main(String[] args) {
        testRedBlackTree();
    }

    private static void testRedBlackTree() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        String cmp = "19 (18 (null () ()) (null () ())) (20 (null () ()) (null () ()))";
        for (int i = 18; i < 21; i++) {
            tree.add(i);
        }
        assertEquals("RedBlackTreeTest.testRedBlackTree", cmp, tree.toString((NodeImpl<Integer>) tree.root()));
    }
}
