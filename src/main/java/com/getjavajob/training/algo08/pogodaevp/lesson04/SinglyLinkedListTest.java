package com.getjavajob.training.algo08.pogodaevp.lesson04;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testAddToSinglyList();
        testReverseSinglyList();
    }

    private static void testAddToSinglyList() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        singlyLinkedList.add(12);
        singlyLinkedList.add(14);
        singlyLinkedList.add(16);
        ArrayList<Integer> result = new ArrayList<>();
        result.add(16);
        result.add(14);
        result.add(12);
        ArrayList<Integer> cmp = new ArrayList<>();
        cmp.add(singlyLinkedList.head.val);
        cmp.add(singlyLinkedList.head.next.val);
        cmp.add(singlyLinkedList.head.next.next.val);
        assertEquals("SinglyLinkedListTest.testAddToSinglyList", cmp, result);
    }

    private static void testReverseSinglyList() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        singlyLinkedList.add(12);
        singlyLinkedList.add(14);
        singlyLinkedList.add(16);
        singlyLinkedList.reverse();

        ArrayList<Integer> result = new ArrayList<>();
        result.add(12);
        result.add(14);
        result.add(16);
        ArrayList<Integer> cmp = new ArrayList<>();
        cmp.add(singlyLinkedList.head.val);
        cmp.add(singlyLinkedList.head.next.val);
        cmp.add(singlyLinkedList.head.next.next.val);
        assertEquals("SinglyLinkedListTest.testReverseSinglyList", cmp, result);
    }
}
