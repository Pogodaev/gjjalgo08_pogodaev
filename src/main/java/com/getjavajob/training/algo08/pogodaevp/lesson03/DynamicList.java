package com.getjavajob.training.algo08.pogodaevp.lesson03;

/**
 * Created by paul on 09.05.16.
 */
public interface DynamicList<E> {
    boolean add(E e);

    void add(int i, E e);

    E set(int i, E e);

    E get(int i);

    E remove(int i);

    //   boolean remove(E e);

    int size();

//    int indexOf(E e);

//    boolean contains(E e);

    E[] toArray();
}
