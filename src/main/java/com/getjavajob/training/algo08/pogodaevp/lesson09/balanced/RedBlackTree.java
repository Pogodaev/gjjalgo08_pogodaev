package com.getjavajob.training.algo08.pogodaevp.lesson09.balanced;

import com.getjavajob.training.algo08.pogodaevp.lesson07.Node;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E> extends BalanceableTree<E> {
    @Override
    public String toString(NodeImpl<E> node) {
        String str = "";
        if (node == null) {
            return str;
        }
        str += node.getElement();
        str += " (" + toString(node.left) + ") (" + toString(node.right) + ")";
        return str;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> castNode = (NodeImpl<E>) n;

        if (compare(castNode.getElement(), e) >= 0) {
            if (castNode.left == null) {
                afterElementAdded(addLeft(n, e));
            } else {
                afterElementAdded(add(castNode.left, e));
            }
        }

        if (compare(castNode.getElement(), e) < 0) {
            if (castNode.right == null) {
                afterElementAdded(addRight(n, e));
            } else {
                afterElementAdded(add(castNode.right, e));
            }
        }
        return castNode;
    }

    public void add(E val) {
        boolean left = false;
        NodeImpl<E> node = root;
        NodeImpl<E> parent = null;
        if (root == nil) {
            root = new NodeImpl<>(val);
            root.parent = nil;
        } else {
            while (node != nil) {
                if (compare(val, node.getElement()) < 0) {
                    parent = node;
                    node = node.left;
                    left = true;
                } else {
                    parent = node;
                    node = node.right;
                    left = false;
                }
            }
            if (left) {
                parent.left = new NodeImpl<>(val);
                parent.left.parent = parent;
                afterElementAdded(parent.left);
            } else {
                parent.right = new NodeImpl<>(val);
                parent.right.parent = parent;
                afterElementAdded(parent.right);
            }
        }
    }

    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> removed;
        Node<E> sNode = (NodeImpl<E>) treeSearch(root, n.getElement());
        removed = (NodeImpl<E>) sNode;
        if (removed == null) {

            return null;
        }
        E prev = removed.getElement();
        NodeImpl<E> tmp = removed;
        boolean initColor = tmp.color;
        NodeImpl<E> node = null;
        if (removed.left == nil) {
            node = removed.right;
            changeRef(removed, removed.right);
        } else if (removed.right == nil) {
            node = removed.left;
            changeRef(removed, removed.left);
        } else {
            tmp = minNodeInTree(removed.right);
            initColor = tmp.color;
            node = tmp.right;
            if (tmp.parent == removed) {
                node.parent = tmp;
            } else {
                changeRef(tmp, tmp.right);
                tmp.right = removed.right;
                removed.right.parent = tmp;
            }
            changeRef(removed, tmp);
            tmp.left = removed.left;
            removed.left.parent = tmp;
            tmp.color = removed.color;
        }
        if (initColor == true) {
            afterElementRemoved(node);
        }
        return prev;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> rbtNode = (NodeImpl<E>) n;
        if (rbtNode.left == null) {
            NodeImpl<E> newNode = new NodeImpl<E>(e);
            rbtNode.left = newNode;
            newNode.parent = rbtNode;
            afterElementAdded(newNode);
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> rbtNode = (NodeImpl<E>) n;
        if (rbtNode.right == null) {
            NodeImpl<E> newNode = new NodeImpl<>(e);
            rbtNode.right = newNode;
            newNode.parent = rbtNode;
            afterElementAdded(newNode);
            return newNode;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private boolean isBlack(Node<E> n) {
        return ((NodeImpl) n).color == true;
    }

    private boolean isRed(Node<E> n) {
        return ((NodeImpl) n).color == false;
    }

    private boolean colorOf(Node<E> n) {
        return n == null ? true : ((NodeImpl) n).color;
    }

    private void makeBlack(Node<E> n) {
        ((NodeImpl<E>) n).color = true;
    }

    private void makeRed(Node<E> n) {
        ((NodeImpl) n).color = false;
    }


    protected void afterElementAdded(Node<E> n) {
        NodeImpl<E> node = (NodeImpl<E>) n;
        makeRed(node);
        while (isRed(node.parent)) {
            if (node.parent != null && node.parent.parent != null && node.parent == node.parent.parent.left) {
                NodeImpl<E> uncle = (NodeImpl<E>) node.parent.parent.right;
                if (uncle != null && isRed(uncle)) {
                    makeBlack(node.parent);
                    makeBlack(uncle);
                    makeRed(node.parent.parent);
                    node = node.parent.parent;
                    continue;
                } else if ((uncle == null || isBlack(uncle)) && isRed(node.parent)) {
                    if (node == node.parent.right) {
                        rotateTwice(node);
                    } else {
                        makeBlack(node.parent);
                        makeRed(node.parent.parent);
                        rotate(node);
                    }
                }
            } else {
                NodeImpl<E> uncle = node.parent.parent.left;
                if (uncle != nil && isRed(uncle)) {
                    makeBlack(node.parent);
                    makeBlack(uncle);
                    makeRed(node.parent.parent);
                    node = node.parent.parent;
                    continue;
                } else if ((uncle == nil || isBlack(uncle)) && isRed(node.parent)) {
                    if (node == node.parent.left) {
                        rotateTwice(node);
                    } else {
                        makeBlack(node.parent);
                        makeRed(node.parent.parent);
                        rotate(node.parent);
                    }
                }
            }
        }
        makeBlack(root);
    }
}
