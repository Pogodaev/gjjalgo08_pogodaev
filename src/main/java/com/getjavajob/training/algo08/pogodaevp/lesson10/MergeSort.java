package com.getjavajob.training.algo08.pogodaevp.lesson10;

/**
 * Created by paul on 12.06.16.
 */
public class MergeSort {

    private static void merge(int[] first, int[] second, int[] result) {
        int fst = 0;
        int sec = 0;
        int j = 0;

        while (fst < first.length && sec < second.length) {
            if (first[fst] < second[sec]) {
                result[j] = first[fst];
                fst++;
            } else {
                result[j] = second[sec];
                sec++;
            }
            j++;
        }

        System.arraycopy(first, fst, result, j, first.length - fst);
        System.arraycopy(second, sec, result, j, second.length - sec);
    }

    public static int[] mergeSort(int[] list) {
        if (list.length <= 1) {
            return list;
        }

        int[] first = new int[list.length / 2];
        int[] second = new int[list.length - first.length];
        System.arraycopy(list, 0, first, 0, first.length);
        System.arraycopy(list, first.length, second, 0, second.length);

        mergeSort(first);
        mergeSort(second);

        merge(first, second, list);
        return list;
    }
}
