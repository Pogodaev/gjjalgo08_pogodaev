package com.getjavajob.training.algo08.pogodaevp.lesson04;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 23.05.16.
 */
public class CollectionTest {

    public static void main(String[] args) {
        testAdd();
        testAddAll();
        testClear();
        testContains();
        testContainsAll();
        testEquals();
        testHashCode();
        testIsEmpty();
        testRemove();
        testRemoveAll();
        testRetainAll();
        testSize();
        testToArray();
    }

    private static void testToArray() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(13);
        list.add(14);
        Object[] my = list.toArray();
        assertEquals("CollectionTest.testToArray", 13, (int) my[0]);
    }

    private static void testSize() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(13);
        list.add(14);
        assertEquals("CollectionTest.testSize", 2, list.size());
    }

    private static void testRetainAll() {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> list1 = new ArrayList<>();
        list.add(13);
        list1.add(13);
        list.add(14);
        assertEquals("CollectionTest.testRetainAll", true, list.retainAll(list1));
    }

    private static void testRemoveAll() {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> list1 = new ArrayList<>();
        list.add(13);
        list1.add(13);
        list.add(14);
        assertEquals("CollectionTest.testRemoveAll", true, list.removeAll(list1));
    }

    private static void testRemove() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(13);
        list.add(14);
        assertEquals("CollectionTest.testRemove", 13, list.remove(0));
    }

    private static void testIsEmpty() {
        ArrayList<Integer> list = new ArrayList<>();
        assertEquals("CollectionTest.testIsEmpty", true, list.isEmpty());
    }

    private static void testHashCode() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(17);
        list.add(19);
        assertEquals("CollectionTest.testHashcode", 1507, list.hashCode());
    }

    private static void testEquals() {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(17);
        list1.add(19);
        list.add(17);
        list.add(19);
        assertEquals("CollectionTest.testEquals", true, list.equals(list1));
    }

    private static void testContainsAll() {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(17);
        list1.add(19);
        list.add(17);
        list.add(19);
        assertEquals("CollectionTest.testContainsAll", true, list.containsAll(list1));
    }

    private static void testContains() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(17);
        list.add(19);
        assertEquals("CollectionTest.testContains", true, list.contains(17));
        assertEquals("CollectionTest.testContainsFalse", false, list.contains(18));
    }

    private static void testClear() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(17);
        list.add(19);
        list.clear();
        assertEquals("CollectionTest.testClear", 0, list.size());
    }

    private static void testAddAll() {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> listAll = new ArrayList<>();
        listAll.add(17);
        listAll.add(19);
        assertEquals("CollectionTest.testAddAll", true, list.addAll(listAll));
    }

    private static void testAdd() {
        ArrayList<Integer> list = new ArrayList<>();
        assertEquals("CollectionTest.testAdd", true, list.add(13));
    }
}
