package com.getjavajob.training.algo08.pogodaevp.lesson03;

import com.getjavajob.training.algo08.pogodaevp.util.Assert;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.fail;

/**
 * Created by paul on 09.05.16.
 */
public class DynamicArrayTest {
    public static void main(String[] args) {
        testBooleanAdd();
        testAddIntoBegginig();
        testAddIntoMiddle();
        testAddIntoEnd();
        testSet();
        testGet();
        testRemoveFromBegin();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testBooleanRemove();
        testIndexOf();
    }


    public static void testBooleanAdd() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 14; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.add(16, 14);
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testBooleanAddTrue", msg, e.getClass().getSimpleName());
        }
    }

    public static void testAddIntoBegginig() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }

        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.add(0, 15);
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testBooleanAddTrue", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("DynamicArrayTest.testBooleanAddTrue", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void testAddIntoMiddle() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.add(7, 15);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testAddIntoMiddle", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("DynamicArrayTest.testAddIntoMiddle", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void testAddIntoEnd() {
        DynamicArray<Number> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.add(14, 15);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testAddIntoEnd", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("DynamicArrayTest.testAddIntoEnd", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void testSet() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.set(16, 7);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testSet", msg, e.getClass().getSimpleName());
        }

    }

    public static void testGet() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.get(17);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testGet", msg, e.getClass().getSimpleName());
        }
        //   assertEquals("DynamicArrayTest.testGetTrue", 4, intDynamicArray.get(4));
    }

    public static void testRemoveFromBegin() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.remove(0);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRemoveFromBegin", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("DynamicArrayTest.testRemoveFromBegin", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void testRemoveFromMiddle() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.remove(7);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRemoveFromMiddleFailed", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("DynamicArrayTest.testRemoveFromMiddleFailed", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void testRemoveFromEnd() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.remove(14);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRemoveFromEndFailed", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("DynamicArrayTest.testRemoveFromEndFailed", "AssertionError", e.getClass().getSimpleName());
        }
    }

    public static void testBooleanRemove() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        String msg = "ArrayIndexOutOfBoundsException";
        try {
            intDynamicArray.remove(17);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testBooleanRemove", msg, e.getClass().getSimpleName());
        }
        assertEquals("DynamicArrayTest.testBooleanRemoveOk", true, intDynamicArray.remove(new Integer(4)));
    }

    public static void testIndexOf() {
        DynamicArray<Integer> intDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            intDynamicArray.add(i);
        }
        assertEquals("DynamicArrayTest.testIndexOf", 5, intDynamicArray.indexOf(new Integer(5)));
        assertEquals("DynamicArrayTest.testIndexOfFail", -1, intDynamicArray.indexOf(new Integer(17)));
    }

}
