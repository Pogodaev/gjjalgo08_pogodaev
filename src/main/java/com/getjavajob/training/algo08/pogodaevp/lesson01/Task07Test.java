package com.getjavajob.training.algo08.pogodaevp.lesson01;

import static com.getjavajob.training.algo08.pogodaevp.lesson01.Task07.*;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 09.05.16.
 */
public class Task07Test {

    public static void main(String[] args) {
        assertEquals("Task06Test.firstArithSwap", "15 19", firstArithSwap(19, 15));
        assertEquals("Task06Test.secondArithSwap", "19 15", secondArithSwap(15, 19));
        assertEquals("Task06Test.firstBitwiseSwap", "15 19", firstBitwiseSwap(19, 15));
        assertEquals("Task06Test.secondBitwiseSwap", "19 15", secondBitwiseSwap(15, 19));
    }
}
