package com.getjavajob.training.algo08.pogodaevp.lesson06;

import java.util.ArrayList;
import java.util.HashMap;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 27.07.16.
 */
public class MatrixTaskTest {
    public static void main(String[] args) {
        testMatrixFill();
    }

    private static void testMatrixFill() {
        MatrixTask matrix = new MatrixTask(new HashMap<KeyMatrix, Integer>());
        int N = 1000000;
        ArrayList<Integer> compare = new ArrayList<>();
        ArrayList<Integer> res = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            matrix.set(i, N, i);
            compare.add(i);
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(matrix.get(i, N));
        }
        assertEquals("MatrixTaskTest.testMatrixFill", compare, res);
    }
}
