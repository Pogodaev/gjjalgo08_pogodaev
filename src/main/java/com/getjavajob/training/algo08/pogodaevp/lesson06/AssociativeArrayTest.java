package com.getjavajob.training.algo08.pogodaevp.lesson06;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 26.07.16.
 */
public class AssociativeArrayTest {
    public static void main(String[] args) {
        testAddToArray();
        testAddNullToArray();
        testRemove();

    }

    private static void testRemove() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "First");
        assertEquals("AssociativeArrayTest.testRemove", "First", array.remove(1));
    }

    private static void testAddToArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "First");
        assertEquals("AssociativeArrayTest.testAddToArray", "First", array.get(1));
    }

    private static void testAddNullToArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(null, "First");
        assertEquals("AssociativeArrayTest.testAddNullToArray", "First", array.get(null));
    }
}
