package com.getjavajob.training.algo08.pogodaevp.lesson08.search;

import com.getjavajob.training.algo08.pogodaevp.lesson07.Node;

import java.util.Comparator;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class BinarySearchTreeTest {

    public static void main(String[] args) {
        testCompare();
        testBinarySearch();
    }

    public static void testCompare() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return integer - t1;
            }
        });
        assertEquals("BinarySearchTreeTest.testCompareMore", true, bst.compare(12, 3) > 0);
        assertEquals("BinarySearchTreeTest.testCompareLess", false, bst.compare(3, 12) > 0);
        assertEquals("BinarySearchTreeTest.testCompareEquals", true, bst.compare(12, 12) == 0);
    }

    public static void testBinarySearch() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return integer - t1;
            }
        });

        Node<Integer> node = bst.addRoot(12);
        bst.addLeft(bst.root(), 10);
        bst.addRight(bst.root(), 19);
        System.out.println(bst.treeSearch(node, 19).getElement());
    }
}
