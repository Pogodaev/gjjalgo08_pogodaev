package com.getjavajob.training.algo08.pogodaevp.lesson09;

import java.util.Arrays;
import java.util.Collections;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class NavigableSetTest {

    static NavigableSet<Integer> set = new TreeSet<>(Arrays.asList(1, 2, 3));

    public static void main(String[] args) {
        testCeiling();
        testDescendingSet();
        testFloor();
        testHeadSet();
        testHigher();
        testLower();
        testPollFirst();
        testPollLast();
        testSubSet();
        testTailSet();
    }

    private static void testTailSet() {
        NavigableSet<Integer> sub = new TreeSet<>(Arrays.asList(2, 3));
        assertEquals("NavigableSetTest.testTailSet", sub, set.tailSet(2, true));
    }

    private static void testSubSet() {
        NavigableSet<Integer> sub = new TreeSet<>(Arrays.asList(2, 3));
        assertEquals("NavigableSetTest.testSubSet", sub, set.subSet(2, true, 3, true));
    }

    private static void testPollLast() {
        assertEquals("NavigableSetTest.testPollLast", 1, set.pollFirst());
        set.add(1);
    }

    private static void testPollFirst() {
        assertEquals("NavigableSetTest.testPollFirst", 1, set.pollFirst());
        set.add(1);
    }

    private static void testLower() {
        assertEquals("NavigableSetTest.testLower", 1, set.lower(2));
    }

    private static void testHigher() {
        assertEquals("NavigableSetTest.testHigher", 3, set.higher(2));
    }

    private static void testHeadSet() {
        NavigableSet<Integer> head = new TreeSet<>(Arrays.asList(1, 2, 3));
        assertEquals("NavigableSetTest.testHeadSet", head, set.headSet(4, true));
    }

    private static void testFloor() {
        assertEquals("NavigableSetTest.testFloor", 3, set.floor(4));
    }

    private static void testDescendingSet() {
        NavigableSet<Integer> reverse = new TreeSet<>(Collections.reverseOrder());
        reverse.addAll(set);
        assertEquals("NavigableSetTest.testDescendingSet", reverse, set.descendingSet());
    }

    private static void testCeiling() {
        assertEquals("NavigableSetTest.testCeiling", 2, set.ceiling(2));
    }
}
