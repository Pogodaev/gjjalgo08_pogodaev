package com.getjavajob.training.algo08.pogodaevp.lesson05;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class LinkedListStackTest {

    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    private static void testRemove() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        stack.push(13);
        stack.push(12);
        stack.push(11);
        stack.pop();
        assertEquals("LinkedListStackTest.testRemove", "12 13 ", stack.toString());
    }

    private static void testAdd() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        stack.push(13);
        stack.push(12);
        stack.push(11);
        assertEquals("LinkedListStackTest.testAdd", "11 12 13 ", stack.toString());
    }
}
