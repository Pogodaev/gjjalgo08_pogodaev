package com.getjavajob.training.algo08.pogodaevp.lesson01;

import static com.getjavajob.training.algo08.pogodaevp.lesson01.Task06.*;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 09.05.16.
 */
public class Task06Test {
    public static void main(String[] args) {
        assertEquals("Task06Test.getShiftDegree", "1000", Integer.toBinaryString(getShiftDegree(3)));
        assertEquals("Task06Test.getTwoDegreeNumbersSum", "10100", Integer.toBinaryString(getTwoDegreeNumbersSum(3, 2)));
        assertEquals("Task06Test.resetLowerBits", "11100", Integer.toBinaryString(resetLowerBits(31, 2)));
        assertEquals("Task06Test.setNbit", "111", Integer.toBinaryString(setNbit(5, 1)));
        assertEquals("Task06Test.invertBit", "101", Integer.toBinaryString(invertBit(7, 1)));
        assertEquals("Task06Test.zeroSetNthBit", "1011", Integer.toBinaryString(zeroSetNthBit(15, 2)));
        assertEquals("Task06Test.getNLowerBits", "11", Integer.toBinaryString(getNLowerBits(15, 1)));
        assertEquals("Task06Test.getNthBit", "0", Integer.toBinaryString(getNthBit(11, 2)));
        assertEquals("Task06Test.getBinaryRepresentation", "100111", getBinaryRepresentation((byte) 39));
    }
}
