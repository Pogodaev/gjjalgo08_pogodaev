package com.getjavajob.training.algo08.pogodaevp.lesson04;

/**
 * Created by paul on 23.05.16.
 */
public class SinglyLinkedList<E> {
    public Node<E> head;

    public void add(E e) {
        Node node = new Node();
        node.val = e;
        if (head == null) {
            head = node;
        } else {
            node.next = head;
            head = node;
        }
    }

    void reverse() {
        Node counter = head;
        Node previous = null;
        Node incoming = null;

        while (counter != null) {
            incoming = counter.next;   // store incoming item

            swap(previous, counter);
            previous = counter;             // increment also pre

            counter = incoming;        // increment current
        }
        head = previous;
    }

    void swap(Node a, Node b) {
        b.next = a;
    }

    static class Node<E> {
        public Node<E> next;
        public E val;
    }

}

