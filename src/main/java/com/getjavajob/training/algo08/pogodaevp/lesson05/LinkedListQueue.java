package com.getjavajob.training.algo08.pogodaevp.lesson05;

/**
 * Created by paul on 23.05.16.
 */
public class LinkedListQueue<E> extends AbstractQueue<E> {
    private Node<E> first;    // beginning of queue
    private Node<E> last;     // end of queue
    private int count;

    static class Node<E> {
        public Node<E> next;
        public E val;
    }

    @Override
    public boolean add(E e) {
        Node<E> old = last;
        last = new Node<E>();
        last.val = e;
        last.next = null;
        if (first == null) {
            first = last;
        } else {
            old.next = last;
        }
        count++;
        return true;
    }

    @Override
    public E remove() {
        if (first != null) {
            E e = first.val;
            first = first.next;
            count--;
            if (first == null) {
                last = null;
            }
            return e;
        }
        return null;
    }

    @Override
    public String toString() {
        String str = "";
        Node<E> node = first;

        while (node != null) {
            str += node.val + " ";
            node = node.next;
        }
        return str;
    }
}
