package com.getjavajob.training.algo08.pogodaevp.lesson04;

import com.getjavajob.training.algo08.pogodaevp.util.Assert;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.pogodaevp.util.Assert.fail;

/**
 * Created by paul on 19.05.16.
 */
public class DoublyLinkedListTest {
    public static void main(String[] args) {
        addToPosition();
        addToDll();
        removeFromDllbyIndex();
        removeFromDllByVal();
        getFromDllByIndex();
    }

    public static void addToPosition() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        String msg = "IndexOutOfBoundsException";

        try {
            intDll.add(0, 1);
            assertEquals("DoublyLinkedListTest.addToPosition", 1, intDll.size());
            intDll.add(2, 2);
            Assert.fail(msg);
        } catch (Exception e) {
            assertEquals("DoublyLinkedListTest.addToPositionFail", msg, e.getClass().getSimpleName());
        }
    }

    public static void addToDll() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        assertEquals("DoublyLinkedListTest.addToDll", true, intDll.add(1));
    }

    public static void removeFromDllbyIndex() {
        DoublyLinkedList<String> intDll = new DoublyLinkedList<>();
        intDll.add("1");
        assertEquals("DoublyLinkedListTest.removeFromDllbyIndexFail", "1", intDll.remove(0));
    }

    public static void removeFromDllByVal() {
        DoublyLinkedList<String> intDll = new DoublyLinkedList<>();
        intDll.add("1");
        intDll.add("2");
        assertEquals("DoublyLinkedListTest.removeFromDllByValFail", false, intDll.remove("3uk"));
        assertEquals("DoublyLinkedListTest.removeFromDllByValOk", true, intDll.remove("1"));
    }

    public static void getFromDllByIndex() {
        DoublyLinkedList<String> intDll = new DoublyLinkedList<>();
        intDll.add("1");
        String msg = "IndexOutOfBoundsException";

        try {
            assertEquals("DoublyLinkedListTest.getFromDllByIndexFail", "1", intDll.get(0));
            intDll.get(1);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DoublyLinkedListTest.getFromDllByIndexFail", msg, e.getClass().getSimpleName());
        }
    }
}
