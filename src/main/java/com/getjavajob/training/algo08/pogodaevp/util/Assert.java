package com.getjavajob.training.algo08.pogodaevp.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by paul on 09.05.16.
 */
public class Assert {

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        boolean b = true;

        if (expected.length == actual.length) {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i] != actual[i]) {
                    b = false;
                }
            }
        }

        if (b) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected ");
            for (double k : expected) {
                System.out.print(k + " ");
            }

            System.out.println("\n actual ");
            for (double k : actual) {
                System.out.print(k + " ");
            }
        }
    }

    public static void assertEquals(String testName, String[] expected, String[] actual) {
        boolean b = true;

        if (expected.length == actual.length) {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i].equals(actual[i])) {
                    b = false;
                }
            }
        }

        if (b) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected ");
            for (String k : expected) {
                System.out.print(k + " ");
            }

            System.out.println("\n actual ");
            for (String k : actual) {
                System.out.print(k + " ");
            }
        }
    }

    public static void assertEquals(String testName, Map<?, ?> expected, Map<?, ?> actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, Collection<?> expected, Collection<?> actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, Map.Entry<?, ?> expected, Map.Entry<?, ?> actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        boolean b = true;

        if (expected.length == actual.length) {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i] != actual[i]) {
                    b = false;
                }
            }
        }

        if (b) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected ");
            for (int k : expected) {
                System.out.print(k + " ");
            }

            System.out.println("\n actual ");
            for (int k : actual) {
                System.out.print(k + " ");
            }
        }
    }

    public static void assertEquals(String testName, Integer[] expected, Integer[] actual) {
        boolean b = true;

        if (expected.length == actual.length) {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i] != actual[i]) {
                    b = false;
                }
            }
        }

        if (b) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected ");
            for (int k : expected) {
                System.out.print(k + " ");
            }

            System.out.println("\n actual ");
            for (int k : actual) {
                System.out.print(k + " ");
            }
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        boolean b = true;

        if (expected.length == actual.length) {
            for (int i = 0; i < expected.length; i++) {
                for (int j = 0; j < expected[i].length; j++) {
                    if (expected[i][j] != actual[i][j]) {
                        b = false;
                    }
                }
            }
        } else {
            b = false;
        }

        if (b) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\n expected ");
            for (int k[] : expected) {
                for (int l : k) {
                    System.out.print(l + " ");
                }
                System.out.println();
            }

            System.out.println("actual");
            for (int k[] : actual) {
                for (int l : k) {
                    System.out.print(l + " ");
                }
                System.out.println();
            }
        }
    }

    public static void fail(String msg) {
        throw new AssertionError(msg);
    }

    public static void equals(String msg, String error) {
        if (msg.equals(error)) {
            System.out.println("Test passed");
        } else {
            System.out.println("Test failed");
        }
    }

    public static void assertEquals(String testName, List<String> expected, List<String> actual) {
        boolean flag = true;

        if (expected != null && actual != null && expected.size() == actual.size() && expected.size() > 0) {
            for (int i = 0; i < actual.size(); i++) {
                if (!expected.get(i).equals(actual.get(i))) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }
        if (flag) {
            System.out.println(testName + " passed");
        } else {
            String ex = new String();
            String ac = new String();
            for (String s : expected) {
                ex = ex + s;
            }
            for (String s : actual) {
                ac = ac + s;
            }
            System.out.println(testName + " failed: expected " + ex + ", actual " + ac);
        }
    }

    public static void assertEquals(String testName, ArrayList<Integer> expected, ArrayList<Integer> actual) {
        boolean flag = true;
        if (expected != null && actual != null && expected.size() == actual.size() && expected.size() > 0) {
            for (int i = 0; i < actual.size(); i++) {
                if (expected.get(i) != actual.get(i)) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }

        if (flag) {
            System.out.println("\n" + testName + " passed");
        } else {
            String ex = new String();
            String ac = new String();
            for (Integer s : expected) {
                ex = ex + s + " ";
            }
            for (Integer s : actual) {
                ac = ac + s + " ";
            }
            System.out.println(testName + " failed: expected " + ex + ", actual " + ac);
        }
    }
}
