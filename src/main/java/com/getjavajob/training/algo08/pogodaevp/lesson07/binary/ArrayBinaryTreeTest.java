package com.getjavajob.training.algo08.pogodaevp.lesson07.binary;

import com.getjavajob.training.algo08.pogodaevp.lesson07.Node;

import java.util.ArrayList;
import java.util.Iterator;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        testLeft();
        testRight();
        testRoot();
        testParent();
        testAdd();
        testSet();
        testRemove();
        testIterator();
        testPreorder();
        testInorder();
        testPostorder();
        testBreadthFirst();
    }

    // it also works!!!
    private static void testBreadthFirst() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("F");
            add("B");
            add("G");
            add("A");
            add("D");
            add("I");
            add("C");
            add("E");
            add("H");
        }};
        for (Node<String> n : tree.breadthFirst()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testBreadthFirst", res, cmp);
    }

    // it also works!!!
    private static void testPostorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("A");
            add("C");
            add("E");
            add("D");
            add("B");
            add("H");
            add("I");
            add("G");
            add("F");
        }};
        for (Node<String> n : tree.postOrder()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testPostorder", res, cmp);
    }

    //it works too
    private static void testInorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("A");
            add("B");
            add("C");
            add("D");
            add("E");
            add("F");
            add("G");
            add("H");
            add("I");
        }};
        for (Node<String> n : tree.inOrder()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testInorder", res, cmp);
    }

    // it works! ))))
    private static void testPreorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("F");
            add("B");
            add("A");
            add("D");
            add("C");
            add("E");
            add("G");
            add("I");
            add("H");
        }};
        for (Node<String> n : tree.preOrder()) {
            res.add(n.getElement());
        }
        assertEquals("ArrayBinaryTreeTest.testPreorder", res, cmp);
    }

    public static void testLeft() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>(0, 11);
        tree.addRoot(11);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 12);
        assertEquals("ArrayBinaryTreeTest.testLeft", 12, tree.left(node).getElement());
    }

    public static void testRight() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> parent = new ArrayBinaryTree.NodeImpl<>(0, 1);
        tree.addRoot(1);
        tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 12);
        assertEquals("ArrayBinaryTreeTest.testRight", 12, tree.right(parent).getElement());
    }

    public static void testRoot() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        assertEquals("ArrayBinaryTreeTest.testRoot", 1, tree.root().getElement());
    }

    public static void testParent() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>();
        tree.addRoot(1);

        Node<Integer> ret = tree.addLeft(node, 127);
        assertEquals("ArrayBinaryTreeTest.testParent", 1, tree.parent(ret).getElement());
    }

    public static void testAdd() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>();
        tree.addRoot(1);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        Node<Integer> ret = tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 19);
        assertEquals("ArrayBinaryTreeTest.testAdd", 19, tree.parent(tree.add(ret, 117)).getElement());
    }

    public static void testSet() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        Node<Integer> ret = tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 19);
        assertEquals("ArrayBinaryTreeTest.testSet", 19, tree.set(ret, 67));
        assertEquals("ArrayBinaryTreeTest.testSet", 67, tree.right(tree.root()).getElement());
    }

    public static void testRemove() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        ArrayBinaryTree.NodeImpl<Integer> node = new ArrayBinaryTree.NodeImpl<>();
        tree.addRoot(1);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        Node<Integer> ret = tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 19);
        assertEquals("ArrayBinaryTreeTest.testRemove", 19, tree.remove(ret));
    }

    public static void testIterator() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(12);
        tree.addLeft(new ArrayBinaryTree.NodeImpl<Integer>(), 14);
        tree.addRight(new ArrayBinaryTree.NodeImpl<Integer>(), 17);

        Iterator<Integer> iterator = tree.iterator();
        for (Node n : tree.nodes()) {
            System.out.println(n.getElement());
        }
    }
}
