Addition/remove to/from the beginning test
==============================
DynamicArray.add(e): 15
ArrayList.add(e): 438
------------
DynamicArray.remove(e): 10
ArrayList.remove(e): 9
------------

Addition/remove to/from the middle test
==============================
DynamicArray.add(e): 5
ArrayList.add(e): 5
------------
DynamicArray.remove(e): 4
ArrayList.remove(e): 5
------------
Addition/remove to/from the end test
==============================
DynamicArray.add(e): 0
ArrayList.add(e): 0
------------
DynamicArray.remove(e): 0
ArrayList.remove(e): 0
------------