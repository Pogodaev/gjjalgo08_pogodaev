package com.getjavajob.training.algo08.pogodaevp.lesson09;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class SortedSetTest {

    static SortedSet<Integer> set = new TreeSet<>(Arrays.asList(2, 17, 38, 51));

    public static void main(String[] args) {
        testFirst();
        testHeadSet();
        testLast();
        testSubSet();
        testTailSet();
    }

    private static void testTailSet() {
        SortedSet<Integer> tailSet = new TreeSet<>(Arrays.asList(38, 51));
        assertEquals("SortedSetTest.testTailSet", tailSet, set.tailSet(38));
    }

    private static void testSubSet() {
        SortedSet<Integer> sub = new TreeSet<>(Arrays.asList(2, 17));
        assertEquals("SortedSetTest.testSubSet", sub, set.subSet(2, 38));
    }

    private static void testLast() {
        assertEquals("SortedSetTest.testLast", 51, set.last());
    }

    private static void testHeadSet() {
        SortedSet<Integer> head = new TreeSet<>(Arrays.asList(2, 17));
        assertEquals("SortedSetTest.testHeadSet", head, set.headSet(38));
    }

    private static void testFirst() {
        assertEquals("SortedSetTest.testFirst", 2, set.first());
    }
}
