package com.getjavajob.training.algo08.pogodaevp.util;

/**
 * Created by paul on 09.05.16.
 */
public class StopWatch {

    public static long startTime;
    public static long endTime;

    public static long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public static long getElapsedTime() {
        endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
}
