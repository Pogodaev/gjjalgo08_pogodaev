package com.getjavajob.training.algo08.pogodaevp.lesson06;

import java.util.LinkedHashSet;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class LinkedHashSetTest {
    public static void main(String[] args) {
        testAdd();
    }

    private static void testAdd() {
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        assertEquals("LinkedHashSetTest.addTrue", true, hashSet.add(12));
        assertEquals("LinkedHashSetTest.addFalse", false, hashSet.add(12));
    }
}
