package com.getjavajob.training.algo08.pogodaevp.lesson01;

import java.util.Scanner;

/**
 * Created by paul on 04.05.16.
 */
public class Task06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter n < 31");
        int n = scanner.nextInt();
        System.out.println("Enter m < 31");
        int m = scanner.nextInt();
        System.out.println("Enter a");
        int a = scanner.nextInt();
        System.out.println(getShiftDegree(n));
        System.out.println(getTwoDegreeNumbersSum(n, m));
        System.out.println(resetLowerBits(a, n));
        System.out.println(setNbit(a, n));
        System.out.println(invertBit(a, n));
        System.out.println(zeroSetNthBit(a, n));
        System.out.println(getNLowerBits(a, n));
        System.out.println(getNthBit(a, n));
        System.out.println(getBinaryRepresentation((byte) n));
    }

    /**
     * Calculate 2^n
     *
     * @param n degree
     * @return degree
     */
    public static int getShiftDegree(int n) {
        return 1 << n;
    }

    /**
     * Calculates 2 ^ n + 2 ^ m
     *
     * @param n degree
     * @param m degree
     * @return sum
     */
    public static int getTwoDegreeNumbersSum(int n, int m) {
        return 1 << n | 1 << m;
    }

    /**
     * Reset n lower bits
     *
     * @param a number
     * @param n - quantity of bits to be reset
     * @return number with n lower bits reset
     */
    public static int resetLowerBits(int a, int n) {
        return a & (~1 << n - 1);
    }

    /**
     * Sets bit to "1"
     *
     * @param a number
     * @param n index of bit to be set
     * @return number with seted bit number n
     */
    public static int setNbit(int a, int n) {
        return a | 1 << n;
    }

    /**
     * @param a number
     * @param n index of bit to be invert
     * @return
     */
    public static int invertBit(int a, int n) {
        return a ^ 1 << n;
    }

    /**
     * Sets bit to "0"
     *
     * @param a number
     * @param n index of bit to be zero
     * @return
     */
    public static int zeroSetNthBit(int a, int n) {
        return a & ~(1 << n);
    }

    /**
     * Returns lower bits
     *
     * @param a number
     * @param n quantity
     * @return
     */
    public static int getNLowerBits(int a, int n) {
        return a & ~(~0 << n + 1);
    }

    /**
     * Returns n'th bit of number a
     *
     * @param a number
     * @param n index of bit
     * @return
     */
    public static int getNthBit(int a, int n) {
        if ((a & (1 << n)) == 1 << n) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns Bin representation
     *
     * @param a
     * @return
     */
    public static String getBinaryRepresentation(byte a) {
        StringBuilder sb = new StringBuilder();
        while (a != 0) {
            if (a % 2 == 0) {
                sb.append(0);
            } else {
                sb.append(1);
            }
            a = (byte) (a / 2);
        }
        return sb.reverse().toString();
    }

}
