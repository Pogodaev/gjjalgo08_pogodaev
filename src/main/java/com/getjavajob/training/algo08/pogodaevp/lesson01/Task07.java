package com.getjavajob.training.algo08.pogodaevp.lesson01;

/**
 * Created by paul on 07.05.16.
 */
public class Task07 {
    public static void main(String[] args) {

        System.out.println(firstArithSwap(12, 17));
        System.out.println(secondArithSwap(12, 17));
        System.out.println(firstBitwiseSwap(12, 17));
        System.out.println(secondBitwiseSwap(12, 17));
    }

    public static String firstArithSwap(int a, int b) {
        a = a * b;
        b = a / b;
        a = a / b;
        return a + " " + b;
    }

    public static String secondArithSwap(int a, int b) {
        a = a + b;
        b = a - b;
        a = a - b;
        return a + " " + b;
    }

    public static String firstBitwiseSwap(int a, int b) {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        return a + " " + b;
    }

    public static String secondBitwiseSwap(int a, int b) {
        a = ~a & b | ~b & a;
        b = ~a & b | ~b & a;
        a = ~a & b | ~b & a;
        return a + " " + b;
    }

}
