package com.getjavajob.training.algo08.pogodaevp.lesson09;

import java.util.SortedMap;
import java.util.TreeMap;

import static com.getjavajob.training.algo08.pogodaevp.util.Assert.assertEquals;

/**
 * Created by paul on 10.08.16.
 */
public class SortedMapTest {

    public static void main(String[] args) {
        testFirstKey();
        testHeadMap();
        testLastKey();
        testSubMap();
        testTailMap();
    }

    private static void testTailMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        SortedMap<Integer, String> tail = new TreeMap<>();
        tail.put(2, "two");
        tail.put(3, "three");
        assertEquals("SortedMapTest.testTailMap", tail, map.tailMap(2));
    }

    private static void testSubMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        SortedMap<Integer, String> sub = new TreeMap<>();
        sub.put(1, "one");
        sub.put(2, "two");
        assertEquals("SortedMapTest.testSubMap", sub, map.subMap(1, 3));
    }

    private static void testLastKey() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("SortedMapTest.testLastKey", 3, map.lastKey());
    }

    private static void testHeadMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        SortedMap<Integer, String> head = new TreeMap<>();
        head.put(1, "one");
        head.put(2, "two");
        assertEquals("SortedMapTest.testHeadMap", head, map.headMap(3));
    }

    private static void testFirstKey() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        assertEquals("SortedMapTest.testFirstKey", 1, map.firstKey());
    }
}
