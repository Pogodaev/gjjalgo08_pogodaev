package com.getjavajob.training.algo08.pogodaevp.lesson06;

import java.util.HashMap;
import java.util.Random;

import static com.getjavajob.training.algo08.pogodaevp.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.pogodaevp.util.StopWatch.start;

/**
 * Created by paul on 23.05.16.
 */
public class AssociativeArrayPerfomanceTest {
    public static void main(String[] args) {

        AssociativeArray<Integer, Integer> aa = new AssociativeArray();
        HashMap<Integer, Integer> map = new HashMap<>();

        testPutMethod(aa, map);
        testGetMethod(aa, map);
        testRemoveMethod(aa, map);
        testBorders();
    }

    public static void testPutMethod(AssociativeArray associativeArray, HashMap hashMap) {
        Random random = new Random();
        System.out.println("Addition to the map test");
        System.out.println("==============================");
        start();
        for (int i = 0; i < 1000000; i++) {
            int r = random.nextInt();
            associativeArray.add(i, r);
        }
        System.out.println("AssociativeArray.put(K,V): " + getElapsedTime());
        start();
        for (int i = 0; i < 1000000; i++) {
            int r = random.nextInt();
            hashMap.put(i, r);
        }
        System.out.println("HashMap.put(K,V): " + getElapsedTime());
    }

    public static void testGetMethod(AssociativeArray associativeArray, HashMap hashMap) {
        System.out.println("\nGet from the map test");
        System.out.println("------------");
        start();
        associativeArray.get(50000);
        System.out.println("AssociativeArray.get(K): " + getElapsedTime());
        start();
        hashMap.get(50000);
        System.out.println("HashMap.get(K): " + getElapsedTime());
        System.out.println("------------");
    }

    public static void testRemoveMethod(AssociativeArray associativeArray, HashMap hashMap) {
        System.out.println("\nRemove from the map test");
        System.out.println("------------");
        start();
        associativeArray.remove(50000);
        System.out.println("AssociativeArray.remove(K): " + getElapsedTime());
        start();
        hashMap.remove(50000);
        System.out.println("HashMap.remove(K): " + getElapsedTime());
    }

    public static void testBorders() {
        AssociativeArray<Integer, Integer> associativeArray = new AssociativeArray<>();
        for (int i = 0; i < 20; i++) {
            associativeArray.add(i, i);
        }
        System.out.println(associativeArray.get(14));
        associativeArray.add(null, 3);
        associativeArray.get(null);
        associativeArray.add(1, 8);
    }
}
